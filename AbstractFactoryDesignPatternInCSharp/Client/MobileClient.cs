﻿namespace AbstractFactoryDesignPatternInCSharp
{
    /// <summary>
    /// The 'Client' class
    /// </summary>
    public class MobileClient
    {
        ISmartPhone smartPhone;
        INormalPhone normalPhone;

        public MobileClient(string letter)
        {
            IMobilePhone factory = null;

            if (letter == "S")
               factory = new Samsung();
            else if (letter == "N")
                factory = new Nokia();

            smartPhone = factory.GetSmartPhone();
            normalPhone = factory.GetNormalPhone();
        }

        public string GetSmartPhoneModelDetails()
        {
            return smartPhone.GetModelDetails();
        }

        public string GetNormalPhoneModelDetails()
        {
            return normalPhone.GetModelDetails();
        }
    }
}
