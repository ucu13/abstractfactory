﻿using System;

namespace AbstractFactoryDesignPatternInCSharp
{
    /// <summary>
    /// Abstract Factory Pattern Demo
    /// </summary>
    public class Program
    {
        public static void Main()
        {
           // IMobilePhone nokiaMobilePhone = new Nokia();
            MobileClient nokiaClient = new MobileClient("N");

            Console.WriteLine("********* NOKIA **********");
            Console.WriteLine(nokiaClient.GetSmartPhoneModelDetails());
            Console.WriteLine(nokiaClient.GetNormalPhoneModelDetails());
            
            //IMobilePhone samsungMobilePhone = new Samsung();
            MobileClient samsungClient = new MobileClient("S");

            Console.WriteLine("******* SAMSUNG **********");
            Console.WriteLine(samsungClient.GetSmartPhoneModelDetails());
            Console.WriteLine(samsungClient.GetNormalPhoneModelDetails());

            Console.ReadKey();
        }
    }
}
