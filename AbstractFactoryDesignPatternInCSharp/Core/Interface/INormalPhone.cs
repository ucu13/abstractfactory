﻿namespace AbstractFactoryDesignPatternInCSharp
{
    /// <summary>
    /// The 'AbstractProductB' interface
    /// </summary>
    public interface INormalPhone
    {
        string GetModelDetails();
    }
}
