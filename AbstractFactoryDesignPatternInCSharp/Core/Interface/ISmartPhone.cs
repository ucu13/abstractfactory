﻿namespace AbstractFactoryDesignPatternInCSharp
{
    /// <summary>
    /// The 'AbstractProductA' interface
    /// </summary>
    public interface ISmartPhone
    {
        string GetModelDetails();
    }
}
